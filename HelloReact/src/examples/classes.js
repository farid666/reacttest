class Person {
    constructor(name,year) {
        this.name = name;
        this.year = year;
    }

    calculateAge() {
        return new Date().getFullYear() - this.year;
    }


    greeting(text) {
        return `${text} My name is ${this.name}`;
    }
}


class Student extends Person {

    constructor(name,year,studentNumber) {
        super(name,year);
        this.studentNumber = studentNumber;
    }


    getStudentNumber() {
        return this.getStudentNumber;
    }


    //overloading
    greeting(text) {
        let str = super.greeting(text);
        str+=`My studentNumber is ${this.studentNumber}`;
        return str;
    }

}


var p = new Person('ALi',2000);
var s = new Student("Cinar",2001);
console.log(p.calculateAge());
console.log(p.greeting('Salam'));