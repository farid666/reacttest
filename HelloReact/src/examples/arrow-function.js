//ES%


var helloES5 = function() {
    console.log('Hello from ES5');
}

function heloES5() {
    console.log('Hello from ES5');
}


//ES6

let helloES6 = () => {
    console.log('Hello from ES6');
}


let hello6 = () => console.log('Hello 6');


let multiplyES5 = function(x,y) {
    return x*y;
}


let multiplyES6 = (x,y) => {
    return x*y;
}

let multiple6 = (x,y) => x*y;


let getProductES5 = function(id,name) {
    return {
        id:id,
        name:name
    }
}

//geri object bele dondurukur () icinde
let getProductES6 = (id,name) => ({
    id:id,
    name:name
});



const phones = [
    {name:'Iphone8',price:4000},
    {name:'Iphone7',price:5000},
    {name:'Iphone6',price:6000},
    {name:'Iphone5',price:7000}
];

//map geriye yeni list duzeldir
let priceES5 = phones.map(function(phone) {
    return phone.price;
})

console.log(priceES5);


let priceES6 = phones.map(phone => phone.price);//arrow functionla icinde datalari gotururuk

//filter werte uygun datani goturub geri list dondurur
let filterES5 = phones.filter(function(phone) {
    return phone.price >= 5000;
})


let filterES6 = phones.filter(phone => phone.price>=50000);

//function hyazibn funksiya teyin edende yeni scop yaradir icinde yxuardaki fieldi this ile cagira bimlmirik cunki this hemin  funksiyanin scopllarinin icine aidd
// amma arrow funtionda this ile yuxardaki fieldi cagira bilirik cunki arrwo functiion yeni scope yaratmir




const addES5 = function() {
    console.log(arguments);
}

//arguments arrow functionlarda iwdemir

//arguments birden cox param gonderende icinde saxliya bileceyimiz metoddu
addES5(1,2,3);