
//JSX - Javascript XML
//var template = <h1>My first React app!</h1> buc cur yazan babel bize bunu ozu arxada React.createElement kimi verecek
var container = document.getElementById('root');
var template = <div>
    <h1 id="header1">Hello World</h1>
    <div>Lorem, ipsum dolor.</div>
    <ul>
        <li>Lorem, ipsum dolor.</li>
    </ul>
</div>;

var name = "Samsung s10";
var price = 5000;

var product = {
    name2: "Samsung s11",
    price2: 6000,
    description2: "Cox yaxwi telefon",
    types: ['red', 'blue']
}

function formatPrice(p) {
    if (p.price2) {
        return p.price2 + "AZN";
    } else {
        return "no desc";
    }

}

var template2 = <div>
    <h2 id="product-name">name:{product.name2 ? product.name2 : "no name"}</h2>
    <p id="product-price">price: {formatPrice(product)}</p>
    {/* {product.price2>0 && <p>{product.price2}</p>} burda deyirikki price2 movcuddusa true olsa && den sora iwleye biler gorsensin */}
    <p id="producty-desc">description: iyi bir telefon</p>
    <p>{product.types.length > 0 ? 'there are options ' : 'no options'}</p>
</div>;


var number = 0;

function addOne() {
    number++;
    renderApp();
    console.log("add one");
}

var minusOne = () => {
    number--;
    renderApp();
    console.log("minus one");
}


function renderApp() {
    var template3 = <div>
        <h1>Number: {number}</h1>
        {/* onClick metodunu jsx de C si boyuk yazilir ve function adi iceride moterzesiz verilir javascriptde ama hamsi kicik ve fuksiya moterize ile olur */}
        <button id="btnPlus" className="btnRed" onClick={addOne}>+1</button>
        <button id="btnMinus" className="btnBlue" onClick={minusOne}>-1</button>
    </div>

    // class Counter extends React.Component {
    //     constructor(props) {
    //         super(props);
    //         this.addOne = this.addOne.bind(this)
    //         this.minusOne = this.minusOne.bind(this)
    //         this.reset = this.reset.bind(this)
    //         this.state = {//eger deyiskenleri global teyin etmek isdeyirikse state altinda constructorda edirik
    //             number : 0
    //         }
    //     }


    //     addOne() {
    //         this.setState((prevState) => {
    //             return {
    //                 number : prevState.number+1
    //             }
    //         });
    //     }

    //     minusOne() {
    //         this.setState((prevState) => {//setState etdikde bizim yerimize yeniden render edir menimsedir
    //             return {
    //                 number: prevState -1
    //             }
    //         });
    //     }

    //     reset() {
    //         this.setState({
    //             this.state.number = 0
    //         });
    //     }

    //     render() {
    //         return (
    //             <div>
    //                 <h1>Number: {this.state.number}</h1>
    //                 <button id="btnPlus" className="btnRed" onClick={addOne}>+1</button>
    //                 <button id="btnMinus" className="btnBlue" onClick={minusOne}>-1</button>
    //                 <button onClick={reset}>-1</button>
    //             </div>
    //         );
    //     }
    // }

    // ReactDOM.render(<Counter />,container);

    //ReactDom 
    ReactDOM.render(template3, container);//bu metod ise yuxardaki template yeni h1 HelloWOrld u root idli divin icine yazir
}
//React
// var template = React.createElement(
//     'h1',
//     null,
//     'Hello World'
// );
// //ReactDom 
// ReactDOM.render(template3, container);//bu metod ise yuxardaki template yeni h1 HelloWOrld u root idli divin icine yazir
//const root = createRoot(container);//ReactDOM.render() metodu react 18 de bele yazilir
//root.render(template);


function tick() {
    var element = (
        <div>
            <h2>time is: {new Date().toLocaleTimeString()}</h2>
        </div>
    );


    ReactDOM.render(element, root);
}

setInterval(tick, 1000);
//renderApp();