const container = document.getElementById('root');

const app = {
    title:"Todo APP",
    description:"Lorem,asfddsfdsf",
    items:['item1','item2']
}

// function onFormSubmit(event) {
//     event.preventDefault();
//     let item = event.target.elements.txtItem.value;
//     if (item) {
//         app.items.push(item);
//         event.target.elements.txtItem.value = '';
//         console.log(item);
//         render();
//     }
//     console.log("Form Submitted");
// }

const onFormSubmit = (event) => {
    event.preventDefault();
    let item = event.target.elements.txtItem.value;
    if (item) {
        app.items.push(item);
        event.target.elements.txtItem.value = '';
        console.log(item);
        render();
    }
    console.log("Form Submitted");
}


// function clearItems() {
//     app.items = [];
//     render();
// }

const clearItems = () => {
    app.items = [];
    render();
}

// function render() {
//     let template = (<div>
//         <h1 id="header1">{app.title}</h1>
//         <div>{app.description}</div>
//         <ul>
//             {
//                 app.items.map((item,index) => {
//                     return <li key={index}>{item}</li>
//                 })
//             }
//         </ul>
//         <p><button onClick={clearItems}>Clear Item</button></p>
//         <p>{app.items.length}</p>
//         <form onSubmit={onFormSubmit} >
//             <input type="text" name="txtItem" />
//             <button type="submit" >Add Item</button>
//         </form>
//     </div>);
    
    
//     ReactDOM.render(template, container);
// }

const render = () => {
    let template = (<div>
        <h1 id="header1">{app.title}</h1>
        <div>{app.description}</div>
        <ul>
            {
                app.items.map((item,index) => {
                    return <li key={index}>{item}</li>
                })
            }
        </ul>
        <p><button onClick={clearItems}>Clear Item</button></p>
        <p>{app.items.length}</p>
        <form onSubmit={onFormSubmit} >
            <input type="text" name="txtItem" />
            <button type="submit" >Add Item</button>
        </form>
    </div>);
    
    
    ReactDOM.render(template, container);
}


render();