import React from "react";
import Todo from "./Todo.js";

export default class TodoList extends React.Component {

    constructor(props) {
        super(props); //eger contructoru cagririqsa yazilmalidi mutleq
    }

    render() {
        return (
            <div>
                <ul className="list-group">
                    {
                        this.props.items.map(
                            (item, index) =>
                                <Todo deleteItem={this.props.deleteItem} key={index} item={item} />
                        )
                    }
                </ul>
                {
                    this.props.items.length > 0
                        ?
                        <p><button className="btn btn-outline-danger float-right btn-sm mt-3" onClick={this.props.clearItems}>Clear Item</button></p>
                        :
                        <p className="alert alert-warning">Add Item to start</p>
            }
            </div>
        );
    }
}