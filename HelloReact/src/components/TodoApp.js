import React from 'react';
import Header from './Header.js';
import Action from './Action.js';
import TodoList from './TodoList.js';

export default class TodoApp extends React.Component {

    constructor(props) {
        super(props);
        this.clearItems = this.clearItems.bind(this);
        this.addItem = this.addItem.bind(this);
        this.deleteItem = this.deleteItem.bind(this);
        this.state = {
            items: ["item1", "item2", "item3"]
        }
    }


    //bu metod component yaradilib render edildikden sora iwleyen metoddu biz asagida eger localstroagede data varsa birinci onu aliriq ordan getiririk
    componentDidMount() {
        console.log("Component olusturuldu");
        const json = localStorage.getItem('items');
        const items = JSON.parse(json);

        if (items) {
            this.setState({
                items: items
            });
        }
    }


    //Component update  olduqda isleyir yeniki her hansi state bilgisi ve yaxud props deyisdikde meeselen item sildikde bu isdeyir
    componentDidUpdate(prevProps, prevState) {
        console.log("Component update oldu");
        if (prevState.items.length !== this.state.items.length) {
            const json = JSON.stringify(this.state.items);
            localStorage.setItem('items', json);
        }
    }

    //ReactDOM.render() metodunda eger componenti silib basqasini yazsaq bu meto isleyir
    componentWillUnmount() {
        console.log("Component silindi");
    }


    clearItems() {
        this.setState({
            items: []
        });
    }

    deleteItem(item) {
        this.setState((prevState) => {
            const arr = prevState.items.filter((i) => {
                return item != i
            });
            return {
                items: arr
            }
        })
    }


    addItem(item) {
        if (!item) {
            return 'Bos ola bilmez';
        } else if (this.state.items.indexOf(item) > -1) {
            return 'Tekrar data ola bilmez'
        }

        this.setState((prevState) => {
            return { items: prevState.items.concat(item) };
        });
    }


    render() {
        const app = {
            title: "Todo APP",
            description: "Lorem,asfddsfdsf",
        }

        return (
            <div className='container my-5'>
                <div className='card'>
                    <div className='card-header'>
                        <Header title={app.title} description={app.description} />
                    </div>
                    <div className="card-body">
                        <TodoList items={this.state.items} clearItems={this.clearItems} deleteItem={this.deleteItem} />
                    </div>
                    <div className="card-footer">
                        <Action addItem={this.addItem} />
                    </div>
                </div>
            </div>
        );
    }
}