const path = require('path');

module.exports = {
    entry:'./src/examples/note-app.js',
    output: {
        path: path.resolve(__dirname,'dist'),
        filename:'bundle.js'
    },
    devServer: {
        contentBase:path.resolve(__dirname,'dist'),
        historyApiFallback:true
    },
    module : {
        rules: [
            {
                test:/\.js$/,
                exclude:/node_modules/,
                loader:'babel-loader'
            },
            {
                test:/\.scss$/,
                use: ["style-loader", "css-loader","sass-loader"]
            },
            {
                test: /\.(png|jpe?g|gif)$/i,
                use: [
                  {
                    loader: 'file-loader',
                  },
                ],
              }
        ]
    }
}