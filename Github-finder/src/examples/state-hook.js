import React,{useEffect, useState} from 'react'
import ReactDOM from 'react-dom'

const App = (props) => {
    const [count,setCount] = useState(props.count);
    const [text,setText] = useState(props.text);

    //componentDidMount component ilk ise dusende cagrilir
    useEffect(() => {
        console.log('ComponentDIdMount');
        const countData = localStorage.getItem('count');

        if(countData) {
            setCount(Number(countData));
        }
    },[]) 


    useEffect(() => {
        console.log('count');
        localStorage.setItem('count',count);
    },[count]) 

    useEffect(() => {
        console.log('text');
    },[text]) 


    useEffect(() => {
        console.log('ComponentDIdMount - ComponentDIdUpdate');
    }) 

    return (
        <div>
            <p>Butona {count} kez tikladiniz</p>
            <p>Girilen Text : {text}</p>
            <button onClick={() => setCount(count+1)} >+1</button>
            <button onClick={() => setCount(count-1)} >-1</button>
            <button onClick={() => setCount(props.count)} >Reset</button>
            <input type="text" value={text} onChange={(e) => setText(e.target.value)} />
        </div>
    )
}



App.defaultProps = {
    count:0,
    text:''
}




ReactDOM.render(<App />,document.getElementById('root'));