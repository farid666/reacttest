import React, { useEffect, useReducer, useState } from "react"
import ReactDOM from 'react-dom'
import '../styles/main.scss'



const notesReducer = (state,action) => {
    switch (action.type) {
        case 'POPULATE_NOTES':
            return action.notes;
        case 'ADD_NOTE' :
            return [
                ...state,
                {title: action.title , body : action.body}
            ]
        case 'REMOVE_NOTE' :
            return state.filter((note) => note.title !== action.title)
        default:
            return state;
    }
}

const NoteApp = () => {
    //const [notes, setNotes] = useState([])
    const [notes,dispatch] = useReducer(notesReducer,[])
    const [title, setTitle] = useState('')
    const [body, setBody] = useState('')

    useEffect(() => {
        const noteData = JSON.parse(localStorage.getItem('notes'));
        if (noteData) {
            //setNotes(noteData)
            dispatch({type:'POPULATE_NOTES', notes : noteData})
        }
    },[])

    useEffect(() => {
        localStorage.setItem('notes',JSON.stringify(notes))
    },[notes])

    const addNote = (e) => {
        e.preventDefault();

        if (title) {
            // setNotes([
            //     ...notes,
            //     { title,body},
            // ])
            dispatch({type:'ADD_NOTE',title,body})
            setTitle('')
            setBody('')
        }

    }


    const removeNote = (title) => {
        //setNotes(notes.filter((note) => note.title !== title))
        dispatch({type:'REMOVE_NOTE',title})
    }


    return (
        <div className="container p-5">
            <div className="card mb-3">
                <div className="card-header">
                    Notes
                </div>
                {
                    notes && (
                        <table className="table table-sm table-striped mb-0">
                            <tbody>
                                {
                                    notes.map((note) => (
                                        <tr key={note.title}>
                                            <td style={{ width: '40%' }}>{note.title}</td>
                                            <td>{note.body}</td>
                                            <td style={{ width: '3%' }}>
                                                <button onClick={() => removeNote(note.title)} className="btn btn-warning btn-sm">
                                                    <i className="fas fa-trash-alt"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    ))
                                }
                            </tbody>
                        </table>
                    )
                }
            </div>
            <div className="card mb-3">
                <div className="card-header">
                    Add a New Note
                </div>
                <div className="card-body">
                    <form onSubmit={addNote} className="form-group">
                        <input value={title} onChange={(e) => setTitle(e.target.value)} type="text" className="form-control" placeholder="Title.." />
                        <textarea value={body} onChange={(e) => setBody(e.target.value)} className="form-control my-3" placeholder="Body....."></textarea>
                        <button className="btn btn-primary btn-sm btn-block">Add Note</button>
                    </form>
                </div>
            </div>

        </div>
    )
}



ReactDOM.render(<NoteApp />, document.getElementById('root'));