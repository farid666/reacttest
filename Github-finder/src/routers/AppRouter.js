import React from 'react';
import { BrowserRouter, Route, Switch, Link, NavLink } from "react-router-dom";

const Nav = () => (
    // <nav>
    //     <Link to="/">Home Page</Link>
    //     <Link to="/contact">Contact Page</Link>
    //     <Link to="/about">About Page</Link>
    // </nav>

    <nav>
        <NavLink exact to="/" activeClassName="active">Home Page</NavLink>
        <NavLink exact to="/contact">Contact Page</NavLink>
        <NavLink exact to="/about">About Page</NavLink>
        <NavLink exact to="/products">Products Page</NavLink>
        <NavLink exact to="/products/12?orderby=price">Product Details Page</NavLink>
    </nav>
);

const Header = () => (
    <div>
        <h1>Github Finder</h1>
    </div>
);


const HomePage = () => {
    return (
        <div>Home Page</div>
    )

};

const ContactPage = () => {
    return (
        <div>Contact Page</div>
    )

};

const AboutPage = () => {
    return (
        <div>About Page</div>
    )

};

const NotFoundPage = () => {
    return (
        <div>Not Found 404</div>
    )
};


const ProductsPage = () => (
    <div>
        Products page
    </div>
);

const ProductDetailsPage = (props) => {
    console.log(props)
    return (
        <div>
            Product Details page
            <p>{props.match.params.id}</p>
            <p>{props.location.search}</p>
        </div>      
    )
};

const AppRouter = () => {
    return (
        < BrowserRouter >
            <Header />
            <Nav />
            <Switch>
                <Route exact path='/' component={HomePage} />
                <Route exact path='/contact' component={ContactPage} />
                <Route exact path='/about' component={AboutPage} />
                <Route exact path='/products' component={ProductsPage} />    
                <Route exact path='/products/:id' component={ProductDetailsPage} />
                <Route component={NotFoundPage} />
            </Switch>
        </BrowserRouter >
    )
};


export default AppRouter;