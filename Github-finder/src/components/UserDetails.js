import React, { Component, useEffect } from 'react';
import Loading from './Loading';
import Repos from './Repos';

const UserDetails = (props) => {
    // componentDidMount() {
    //     this.props.getUser(this.props.match.params.login)
    //     this.props.getUserRepos(this.props.match.params.login)
    // }


    useEffect(() => {
        props.getUser(props.match.params.login)
        props.getUserRepos(props.match.params.login)
    }, [])

    //render() {
    const { loading, repos } = props;
    const { name, avatar_url, location, html_url, bio, blog, followers, following, public_repos } = props.user;
    if (loading) {
        return <Loading />
    }
    return (
        <div className='container my-3'>
            <div className="row">
                <div className="col-md-3">
                    <div className="card">
                        <img src={avatar_url} className='card-img-top' />
                        <div className="card-body">
                            <div className="card-text">{name}</div>
                            <p><i className='fas fa-map-marker-alt'></i> {location}</p>
                            <p>
                                <a href={html_url} className='btn btn-block btn-primary btn-sm'>Github Profile</a>
                            </p>
                        </div>
                    </div>

                </div>
                <div className="col-md-9">
                    <div className="card">
                        <div className="card-body">
                            {
                                bio && <>
                                    <h3>About</h3>
                                    <p>{bio}</p>
                                </>
                            }
                            {
                                blog &&
                                <>
                                    <h3>Blog</h3>
                                    <p>{blog}</p>
                                </>
                            }
                            <div>
                                <span className="badge badge-primary m-1">
                                    Followers: {followers}
                                </span>
                                <span className="badge badge-danger m-1">
                                    Following: {following}
                                </span>
                                <span className="badge badge-success m-1">
                                    Public_repos: {public_repos}
                                </span>
                            </div>
                        </div>
                        <ul className="list-group list-group-flush">
                            <Repos repos={repos} />
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    )
}
//}

export default UserDetails
