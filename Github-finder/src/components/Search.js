import React, { useState } from 'react'

//isdesek propsu silib gelenleri {searchUser,showClearButton} kimi ala bilerik
const Search = (props) => {

    const [keyword, setKeyword] = useState('');

    const onChange = (e) => {
        setKeyword(e.target.value);
    }


    const onSubmit = (e) => {
        e.preventDefault();
        if (keyword === '') {
            props.setAlert('Please enter word.', 'danger');
        } else {
            props.searchUsers(keyword);
            setKeyword('');
        }
    }

    return (
        <>
            <div className="container my-3">
                <form onSubmit={onSubmit}>
                    <div className="input-group">
                        <input type="text" className="form-control" value={keyword} onChange={onChange} placeholder='Search User.....' />
                        <div className="input-group-append">
                            <button type='submit' className='btn btn-primary mx-3'>Search</button>
                        </div>
                    </div>
                </form>
                <div className='d-grid gap-2'>
                    {
                        props.showClearButton && <button onClick={props.clearUsers} className='btn btn-secondary btn-sm btn-block mt-2'>Clear Result Items</button>
                    }
                </div>
            </div>
        </>
    )
}


export default Search
