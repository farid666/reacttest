import React, { Component } from 'react';
import {Link} from 'react-router-dom'

//Yucarida {Component} verdikde awagida React.Component yazmaq lazim deyil icinden goturur
export default class User extends Component {
    render() {
        const {login, avatar_url, html_url } = this.props.user;//burda buc ur yazsaq qabaqlarinda this.state yazmaga ehtioyac yoxdu birbasa qey edrik
        return (
            <div className="col-md-3 col-sm-6 col-lg-4 mt-3">
                <img src={avatar_url} className='img-fluid' />
                <div className="card-body">
                    <h5 className='card-title'>{login}</h5>
                    <Link to={`/user/${login}`} className='btn btn-primary btn-sm'>Go Profile</Link>
                </div>
            </div>
        )
    }
}