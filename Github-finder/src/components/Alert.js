import React from 'react'

const Alert = (props) => {
    return (
        <div className="container my-3">
            {props.alert !== null &&
                <div class={`alert alert-${props.alert.type} alert-dismissible fade show`} role="alert">
                    {props.alert.msg}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            }
        </div>
    )
}

export default Alert