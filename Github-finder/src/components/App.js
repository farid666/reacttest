import React, { Component } from 'react';
import Navbar from './Navbar';
import Users from './Users';
import axios from 'axios';
import Search from './Search';
import Alert from './Alert';
import About from './About';
import { BrowserRouter, Route, Switch, Link, NavLink } from 'react-router-dom';
import UserDetails from './UserDetails';

export class App extends Component {

  constructor(props) {
    super(props);
    this.searchUsers = this.searchUsers.bind(this);
    this.clearUsers = this.clearUsers.bind(this);
    this.getUser = this.getUser.bind(this);
    this.getUserRepos = this.getUserRepos.bind(this);
    this.setAlert = this.setAlert.bind(this);
    this.state = {
      loading: false,
      users: [],
      user:{},
      repos:[],
      alert: null
    }
  }

  searchUsers(keyword) {
    this.setState({ loading: true });
    axios.get(`https://api.github.com/search/users?q=${keyword}`)
      .then(res => this.setState({ users: res.data.items, loading: false }));
  }

  getUser(userName) {
    this.setState({loading:true});
    axios.get(`https://api.github.com/users/${userName}`)
    .then(res => this.setState({user:res.data,loading:false}));
  }

  getUserRepos(userName) {
    this.setState({loading:true});
    axios.get(`https://api.github.com/users/${userName}/repos`)
    .then(res => this.setState({repos:res.data,loading:false}))
  }

  clearUsers() {
    this.setState({ users: [] })
  }

  setAlert(msg, type) {
    this.setState({
      alert: { msg, type }
    })
  }


  render() {
    //React.Fragment yazdigimizda eger div isdifade etmek isdemirikse elverislidi ve  UI da gorsenmir 2 Componenti icinde rahat yaziriq

    return (
      <BrowserRouter>

        <Navbar />
        <Alert alert={this.state.alert} />
        <Switch>
          <Route exact path="/" render={props => (
            <React.Fragment>
              <Search setAlert={this.setAlert} searchUsers={this.searchUsers} clearUsers={this.clearUsers} showClearButton={this.state.users.length > 0 ? true : false} />
              <Users loading={this.state.loading} users={this.state.users} />
            </React.Fragment>
          )} />

          <Route path="/about" render={About} />
          <Route path="/user/:login"  render={props => (
            <UserDetails {...props} getUser={this.getUser} user={this.state.user} loading={this.state.loading}  getUserRepos={this.getUserRepos} repos={this.state.repos}/>
          )}/>
        </Switch>
      </BrowserRouter>
    )
  }
}

export default App
