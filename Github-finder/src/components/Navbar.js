import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {Link } from "react-router-dom";

export class Navbar extends Component {
  render() {
    return (
      <nav className='navbar navbar-expand-sm navbar-dark bg-primary'>
        <div className="container">
          <Link exact="true" to="/" className='navbar-brand'>
            <i className={this.props.icon}></i> {this.props.title}
          </Link>

          <div className='collapse navbar-collapse'>
            <ul className='navbar-nav ml-auto'>
              <li className='nav-item'>
                <Link exact="true" to="/about" className="nav-link">About</Link>
              </li>
            </ul>
          </div>
        </div>

      </nav>
    )
  }
}

//Eger bizde ancaq bular lazimdisa onu default bele vere bilerik parametr kimi dg=iger yerden gondermeye ehtiyac yoxdu
Navbar.defaultProps = {
  title: "Github Finder",
  icon: "fab fa-github"
}

//propTypes ile biz validationlar vere bilerik
Navbar.propTypes = {
  title: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired
}

export default Navbar
